# BobbleKeyboardSDK

BobbleKeyboardSDK provides a complete solution to custom keyboard implementation on iOS by helping you create custom keyboard extensions.

## Overview

### Preview

 ![Bobble iOS Keyboard](KeyboardPreview.png)

### Core Features provided by the BobbleKeyboardSDK : 

 1. AI-Driven Word Suggestions, Predictions and Auto-correct functionality.
 2. Dynamic Emoji Suggestions Based on User Input
 3. Stickers Featuring Trending and Personalized Content for Sharing
 4. Shareable Trending and Personalized GIFs
 5. Customized Fonts for Creative Expression.
 6. Personalized Clipboard for Storing and Reusing Frequently Used Texts.
 7. Customize Keyboard Settings to Include Key Border, Auto-Correct, Word Suggestions/Predictions, etc, According to User Preference.

### Additional Features:

 1. Include a top bar above the banner with a customizable height.
 2. Incorporate custom views with the ability to adjust keyboard height based on screen dimensions.
 3. Retrieve the currently typed text.


## Integration Steps

### How to initialise the SDK 

* Drag and the drop the .framework folder into the project
* Make sure to Embedd and Sign the framework. To do so, go to Xcode -> Select the Project Target -> General -> Frameworks and Libraries -> Select Embedd and Sign for BobbleKeyboardSDK.framework
* When we create the custom keyboard extension the base class provided by Xcode is ``KeyboardViewController`` which is inherited from ``UIInputViewController``. Replace the parent class with ``CustomViewController`` to use the SDK

### Following API's are available in BobbleKeyboard SDK

## 1. viewWillAppear(animated)

The custom class that extends ``CustomViewController``, need to override viewWillAppear(animated). viewWillAppear() is called when the keyboard service is triggered by system and before it gets visible to user. This is the starting point for view customization and all customization API calls must start from here.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

    override func viewWillAppear(_ animated: Bool) {
        // Add Customization API calls here
        super.viewWillAppear(animated)
    }
```

## 2. viewWillDisappear(animated)

The custom class that extends ``CustomViewController``, should override viewWillDisappear(animated). viewWillDisappear() is called when keyboard service is triggered by system and its about to hide itself. It also optimizes memory usage effectively, If any view's are added then the code for memory release code for views should be added here.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
 // Add the memory release codes if needed
}
```


## 3. showTopBar(view)

The custom class that extends ``CustomViewController``, can call ``showTopBar(view : UIView)`` to add a top view above the top bar. When ``showTopBar(view)`` is called, the default keyboard view will keep on appearing and new view will be added above the top bar. If already a top bar view is visible, it is replaced by the newly provided top bar view.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    addView()
}

func addView() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 400))
    view.backgroundColor = .red

/* This function will add a view above the keyboard 
 @Params
    view   : Any UIView
 
 */
   showTopBar(view)
   
}
```

## 4. hideTopBar()

The custom class that extends ``CustomViewController``, can call ``hideTopBar()`` to remove the top bar.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    hideTopBar()

}
```

## 5. addCustomView(view, height)

The custom class that extends ``CustomViewController``, can call ``addCustomView(view : UIView , height: Double)`` to add a custom view inside the keyboard view. When ``addCustomView(view, height)`` is called, the default keyboard view is closed and the custom view is shown. If already a custom view is visible, it is replaced by the newly provided custom view.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    addView()

}

func addView() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 400))
    view.backgroundColor = .red

/*
  This function will add the custom view in keyboard by hiding the main keyboard view.
  @Params
     view   : Any UIView
     height : pass it in form of percentage i.e. suppose your view takes 45% of screen height then need to pass 0.45
 */

  addCustomView(view: view, height: 0.45)
   
}

```

## 6. showKeyboardView()

The custom class that extends ``CustomViewController``, can call ``showKeyboardView()`` to land on the default keyboard view. It will also remove any custom views or any view added above the top bar view.

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    addView()
}

func addView() {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 400))
    view.backgroundColor = .red

   addCustomView(view: view, height: 0.45)

// This is example states that suppose you have added the custom view and now want to return back from it into the default keyboard view then we can call this function 

  DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
     self?.showKeyboardView()
  }
   
}

```



## 7. getCurrentText()

The custom class that extends ``CustomViewController``, can call ``getCurrentText()`` to retrive the current typed text

**Example :**

```swift
import BobbleKeyboardSDK
class KeyboardViewController: CustomViewController {

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    getCurrentText()

}
```